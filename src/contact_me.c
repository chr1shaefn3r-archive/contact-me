/*
 * Contact Me
 * Copyright (C) 2013 Christoph Haefner
 */
#include <pebble.h>

Window *window;
BitmapLayer *qrcode;
GBitmap *container;

void window_load(Window *window) {
	Layer *window_layer = window_get_root_layer(window);
	qrcode = bitmap_layer_create(GRect(2, 12, 140, 140));
	bitmap_layer_set_alignment(qrcode, GAlignCenter);
	bitmap_layer_set_background_color(qrcode, GColorWhite);
	container = gbitmap_create_with_resource(RESOURCE_ID_QRCODE);
	bitmap_layer_set_bitmap(qrcode, container);
	layer_add_child(window_layer, bitmap_layer_get_layer(qrcode));
}

void window_unload(Window *window) {
	if(container) {
		gbitmap_destroy(container);
	}
}

void deinit(void) {
	bitmap_layer_destroy(qrcode);
	window_destroy(window);
}

void init(void) {
	window = window_create();
	window_set_window_handlers(window, (WindowHandlers) {
		.load = window_load,
		.unload = window_unload
	});
	window_stack_push(window, true /* Animated */);
}

int main(void) {
	/* Hauptbefehlsschleife */
	init();
	app_event_loop();
	deinit();
}

